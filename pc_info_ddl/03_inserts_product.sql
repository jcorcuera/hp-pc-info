USE [pc_info];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[product] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[product]([id], [product_id], [vendor_id], [description])
SELECT 1, N'5A2A', 1, N'Impresora M426fdw' UNION ALL
SELECT 2, N'142A', 1, N'Impresora M425dn'
COMMIT;
RAISERROR (N'[dbo].[product]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[product] OFF;

