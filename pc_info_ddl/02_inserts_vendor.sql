USE [pc_info];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[vendor] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[vendor]([id], [vendor_id], [description])
SELECT 1, N'03F0', N'HP'
COMMIT;
RAISERROR (N'[dbo].[vendor]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[vendor] OFF;

