﻿using PCInfoRestServiceWCFLibrary.Utils;
using System;
using System.Runtime.Serialization;

namespace PCInfoRestServiceWCFLibrary.DTO
{
    public class NICDTO
    {
        public int id;
        public string description;
        public string macAddress;
        public DateTime? createdDate;
        public DateTime? modifiedDate;

       
        public override bool Equals(object obj)
        {
            var item = obj as NICDTO;

            if (item == null)
            {
                return false;
            }

            return this.macAddress.Equals(item.macAddress);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + macAddress.GetHashCode();
                return hash;
            }
        }
        
    }
}
