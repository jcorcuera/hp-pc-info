﻿using PCInfoRestServiceWCFLibrary.Utils;
using System;
using System.Runtime.Serialization;

namespace PCInfoRestServiceWCFLibrary.DTO
{
    public class PrinterDTO
    {
        public int id;
        public string vendorId;
        public string productId;
        public string serialId;
        public DateTime? createdDate;
        public DateTime? modifiedDate;

        public override bool Equals(object obj)
        {
            var item = obj as PrinterDTO;

            if (item == null)
            {
                return false;
            }

            return this.vendorId.Equals(item.vendorId) && this.productId.Equals(item.productId) && this.serialId.Equals(item.serialId);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + vendorId.GetHashCode();
                hash = hash * 23 + productId.GetHashCode();
                hash = hash * 23 + serialId.GetHashCode();
                return hash;
            }
        }
       
    }
}
