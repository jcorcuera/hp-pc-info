﻿using PCInfoRestServiceWCFLibrary;
using PCInfoRestServiceWCFLibrary.DTO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Management;
using System.Net;
using System.Net.NetworkInformation;
using System.Web.Script.Serialization;

namespace HPUsbPrinterDaemon
{
    public class PCInfoProcessor
    {

        public void processAndSendLocalInformation()
        {
            try
            {
                PCInfoDTO pcInfoDTO = processLocalInformation();
                string serverUrl = ConfigurationManager.AppSettings["serverUrl"];
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(serverUrl);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (StreamWriter streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = new JavaScriptSerializer().Serialize(pcInfoDTO);
                    streamWriter.Write(json);
                    streamWriter.Flush();
                }

                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                PCInfoDTO pcInfoDTOOut = null;
                string result = null;
                using (StreamReader streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
                if (result != null)
                {
                    pcInfoDTOOut = new JavaScriptSerializer().Deserialize<PCInfoDTO>(result);
                }
                if (result != null && !pcInfoDTOOut.operationResult)
                {
                    FileLogger.Log("An error happended in the server: " + result);
                }
            }
            catch(Exception ex)
            {
                FileLogger.Log("Unable to send a request to the server.", ex);
            }
        }

        public PCInfoDTO processLocalInformation()
        {
            PCInfoDTO pcInfoDTO = new PCInfoDTO();

            string machineName = Environment.MachineName;
            pcInfoDTO.hostName = machineName;

            pcInfoDTO.networkCards = new List<NICDTO>();

            ManagementObjectSearcher objMOS = new ManagementObjectSearcher("Select * FROM Win32_NetworkAdapter WHERE MACAddress IS NOT NULL AND PNPDeviceID IS NOT NULL AND PhysicalAdapter = TRUE");
            ManagementObjectCollection objMOC = objMOS.Get();
            string macAddress = String.Empty;
            foreach (ManagementObject objMO in objMOC)
            {
                object tempMacAddrObj = objMO["MACAddress"];
                if (tempMacAddrObj != null) 
                {
                    string manufacturer = objMO["Manufacturer"].ToString();
                    if (manufacturer != "Microsoft")
                    {
                        string pnpDeviceId = objMO["PNPDeviceID"].ToString();
                        string adapterType = objMO["AdapterType"].ToString();
                        string netConnectionID = objMO["netConnectionID"].ToString().ToUpper();
                        string productName = objMO["ProductName"].ToString().ToUpper();
                        if (    
                                !pnpDeviceId.StartsWith("ROOT\\") && adapterType.ToUpper() == "ETHERNET 802.3"
                                && (netConnectionID!= "WI-FI" && !productName.Contains("WIRELESS"))
                                && (netConnectionID != "WIRELESS" && !productName.Contains("WI-FI"))
                            )
                        {
                            NICDTO nicDTO = new NICDTO();
                            nicDTO.macAddress = tempMacAddrObj.ToString();
                            nicDTO.description = objMO["Description"].ToString();
                            pcInfoDTO.networkCards.Add(nicDTO);
                        }
                    }
                }
                objMO.Dispose();
            }


            pcInfoDTO.printers = new List<PrinterDTO>();
            string query = "SELECT * from Win32_USBHub";
            using (ManagementObjectSearcher searcher = new ManagementObjectSearcher(query))
            using (ManagementObjectCollection coll = searcher.Get())
            {
                try
                {
                    foreach (ManagementObject printer in coll)
                    {
                        foreach (PropertyData property in printer.Properties)
                        {
                            if (property.Name.Equals("DeviceID"))
                            {
                                string propertyValue = property.Value.ToString();
                                if (propertyValue.StartsWith("USB\\"))
                                {
                                    string tmpPropertyValue = propertyValue.Replace("USB\\", "");
                                    if (tmpPropertyValue.StartsWith("VID_"))
                                    {
                                        string vId = getToken("VID_", tmpPropertyValue);
                                        string pId = getToken("PID_", tmpPropertyValue);
                                        string serialId = getSerialId(tmpPropertyValue);
                                        if (serialId.Length == 10)
                                        {
                                            PrinterDTO printerDTO = new PrinterDTO();
                                            printerDTO.vendorId = vId;
                                            printerDTO.productId = pId;
                                            printerDTO.serialId = serialId;
                                            pcInfoDTO.printers.Add(printerDTO);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (ManagementException ex)
                {
                    FileLogger.Log("Error happened during the fetching of USB devices.", ex);
                }
            }
            return pcInfoDTO;
        }

        string getToken(string field, string line)
        {
            int fieldIndex = line.IndexOf(field);
            int fieldSize = field.Length;
            string token = null;
            if (fieldIndex != -1)
            {
                token = line.Substring(fieldIndex + fieldSize, 4);
            }
            return token;
        }
         string getSerialId(string line)
        {
            return line.Substring(line.LastIndexOf("\\") + 1);
        }

    }
}
