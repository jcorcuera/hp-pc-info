﻿using PCInfoRestServiceWCFLibrary.Utils;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace PCInfoRestServiceWCFLibrary.DTO
{
    [Serializable]
    public class PCInfoDTO
    {

        public int id;
        public string hostName;
        public List<NICDTO> networkCards;
        public List<PrinterDTO> printers;
        public DateTime? createdDate;
        public DateTime? modifiedDate;
        public bool operationResult;
        public string message;

        public override bool Equals(object obj)
        {
            var item = obj as PCInfoDTO;

            if (item == null)
            {
                return false;
            }

            return this.id.Equals(item.id);
        }

        public override int GetHashCode()
        {
            return this.id.GetHashCode();
        }

    }
}
