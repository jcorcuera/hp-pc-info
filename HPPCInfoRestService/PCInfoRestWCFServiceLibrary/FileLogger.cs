﻿using PCInfoRestServiceWCFLibrary.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace PCInfoRestServiceWCFLibrary
{
    public static class FileLogger
    {
        public static string baseLogFilePath = null;
        public static string logFilePath = null;
        private static readonly object lockObj = new object();
        public readonly static string FILE_SUFFIX_FORMAT = "yyyyMMdd";
        public static DateTime currentTime;
        static FileLogger()
        {
            if (logFilePath == null)
            {
                refreshLogFileName();
            }
        }

        public static void refreshLogFileName()
        {
            DateTime now = DateTime.Now;
            if (logFilePath == null || now.Day != currentTime.Day)
            {
                currentTime = now;
                baseLogFilePath = ConfigurationManager.AppSettings["logFilePath"];
                logFilePath = baseLogFilePath + "_" +  currentTime.ToString(FILE_SUFFIX_FORMAT) + ".log";
            }
        }

        public static void Log(string message)
        {

            lock (lockObj)
            {
                System.IO.StreamWriter sw = System.IO.File.AppendText(logFilePath);
                try
                {
                    string logLine = System.String.Format("{0}: {1}.", System.DateTime.Now.ToString(DateTimeUtil.DATETIME_FORMAT), message);
                    sw.WriteLine(logLine);
                }
                finally
                {
                    sw.Close();
                }
                refreshLogFileName();
            }
        }

        public static void Log(string message, Exception exception)
        {
            string exceptionMsg = GetExceptionDetails(exception);
            Log(message + " " + exceptionMsg);
        }

        public static string GetExceptionDetails(this Exception exception)
        {
            PropertyInfo[] properties = exception.GetType().GetProperties();
            List<string> fields = new List<string>();
            foreach (PropertyInfo property in properties)
            {
                object value = property.GetValue(exception, null);
                fields.Add(String.Format(
                                 "{0} = {1}",
                                 property.Name,
                                 value != null ? value.ToString() : String.Empty
                ));
            }
            return String.Join("\n", fields.ToArray());
        }
    }

}
