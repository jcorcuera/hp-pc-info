﻿using System;
using System.Globalization;

namespace PCInfoRestServiceWCFLibrary.Utils
{
    public class DateTimeUtil
    {
        public readonly static string DATETIME_FORMAT = "dd/MM/yyyy HH:mm:ss";

        public static string serializedDate(DateTime? dateTimeValue)
        {
            string returnValue = null;
            if (dateTimeValue == null)
                returnValue = null;
            else
                returnValue = dateTimeValue.Value.ToString(DateTimeUtil.DATETIME_FORMAT, CultureInfo.InvariantCulture);
            return returnValue;
        }

        public static DateTime? deserializeDate(string dateTimeStrValue)
        {
            DateTime? dateTimeValue = null;
            if (dateTimeStrValue == null || dateTimeStrValue.Equals(""))
                dateTimeValue = null;
            else
                dateTimeValue = DateTime.ParseExact(dateTimeStrValue, DateTimeUtil.DATETIME_FORMAT, CultureInfo.InvariantCulture);
            return dateTimeValue;
        }
    }
}
