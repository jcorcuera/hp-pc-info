﻿using PCInfoRestServiceWCFLibrary.Utils;
using System;
using System.Runtime.Serialization;

namespace PCInfoRestServiceWCFLibrary.DTO
{
    [DataContract]
    public class NICDTO
    {

        [IgnoreDataMember]
        private int id;

        [DataMember]
        private string description;
        [DataMember]
        private string macAddress;

        [IgnoreDataMember]
        private DateTime? createdDate;
        [IgnoreDataMember]
        private DateTime? modifiedDate;

        public string Description
        {
            get
            {
                return description;
            }

            set
            {
                description = value;
            }
        }

        public string MacAddress
        {
            get
            {
                return macAddress;
            }

            set
            {
                macAddress = value;
            }
        }

        public override bool Equals(object obj)
        {
            var item = obj as NICDTO;

            if (item == null)
            {
                return false;
            }

            return this.macAddress.Equals(item.macAddress);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + macAddress.GetHashCode();
                return hash;
            }
        }

        [DataMember(Name = "created")]
        public string Created
        {
            get;
            set;
        }

        [DataMember(Name = "modified")]
        public string Modified
        {
            get;
            set;
        }

        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }

            set
            {
                createdDate = value;
            }
        }

        public DateTime? ModifiedDate
        {
            get
            {
                return modifiedDate;
            }

            set
            {
                modifiedDate = value;
            }
        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        [OnSerializing]
        void OnSerializing(StreamingContext context)
        {
            this.Created = DateTimeUtil.serializedDate(CreatedDate);
            this.Modified = DateTimeUtil.serializedDate(ModifiedDate);
        }

        [OnDeserialized]
        void OnDeserializing(StreamingContext context)
        {
            this.CreatedDate = DateTimeUtil.deserializeDate(Created);
            this.ModifiedDate = DateTimeUtil.deserializeDate(Modified);
        }
    }
}
