﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PCInfoRestServiceWCFLibrary
{
    class VendorProductDTO
    {
        private string vendorId;
        private string productId;

        public string VendorId
        {
            get
            {
                return vendorId;
            }

            set
            {
                vendorId = value;
            }
        }

        public string ProductId
        {
            get
            {
                return productId;
            }

            set
            {
                productId = value;
            }
        }
    }
}
