﻿using PCInfoRestServiceWCFLibrary.DTO;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace PCInfoRestServiceWCFLibrary
{
    [ServiceContract]
    public interface IPCInfoRestService
    {

        [OperationContract]
        [WebInvoke(Method = "POST",
                    RequestFormat = WebMessageFormat.Json,
                    ResponseFormat = WebMessageFormat.Json,
                    UriTemplate = "savePCInfo")]
        PCInfoDTO postData(PCInfoDTO dataItem);
    }
}