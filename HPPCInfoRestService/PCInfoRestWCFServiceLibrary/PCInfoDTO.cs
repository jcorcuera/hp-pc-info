﻿using PCInfoRestServiceWCFLibrary.Utils;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace PCInfoRestServiceWCFLibrary.DTO
{
    [DataContract]
    [Serializable]
    public class PCInfoDTO
    {
        [DataMember]
        private int id;
        [DataMember]
        private string hostName;
        [DataMember]
        private List<NICDTO> networkCards;
        [DataMember]
        private List<PrinterDTO> printers;
        [IgnoreDataMember]
        private DateTime? createdDate;
        [IgnoreDataMember]
        private DateTime? modifiedDate;
        [DataMember]
        private bool operationResult;
        [DataMember]
        private string message;

        public string HostName
        {
            get
            {
                return hostName;
            }

            set
            {
                hostName = value;
            }
        }

        public List<NICDTO> NetworkCards
        {
            get
            {
                return networkCards;
            }

            set
            {
                networkCards = value;
            }
        }

        public List<PrinterDTO> Printers
        {
            get
            {
                return printers;
            }

            set
            {
                printers = value;
            }
        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        [DataMember(Name = "created")]
        public string Created
        {
            get;
            set;
        }

        [DataMember(Name = "modified")]
        public string Modified
        {
            get;
            set;
        }

        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }

            set
            {
                createdDate = value;
            }
        }

        public DateTime? ModifiedDate
        {
            get
            {
                return modifiedDate;
            }

            set
            {
                modifiedDate = value;
            }
        }

        public bool OperationResult
        {
            get
            {
                return operationResult;
            }

            set
            {
                operationResult = value;
            }
        }

        public string Message
        {
            get
            {
                return message;
            }

            set
            {
                message = value;
            }
        }

        [OnSerializing]
        void OnSerializing(StreamingContext context)
        {
            this.Created = DateTimeUtil.serializedDate(CreatedDate);
            this.Modified = DateTimeUtil.serializedDate(ModifiedDate);
        }

        [OnDeserialized]
        void OnDeserializing(StreamingContext context)
        {
            this.CreatedDate = DateTimeUtil.deserializeDate(Created);
            this.ModifiedDate = DateTimeUtil.deserializeDate(Modified);
        }

        public override bool Equals(object obj)
        {
            var item = obj as PCInfoDTO;

            if (item == null)
            {
                return false;
            }

            return this.Id.Equals(item.Id);
        }

        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }
    }
}
