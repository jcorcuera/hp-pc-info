﻿using PCInfoRestServiceWCFLibrary.Utils;
using System;
using System.Runtime.Serialization;

namespace PCInfoRestServiceWCFLibrary.DTO
{
    [DataContract]
    public class PrinterDTO
    {
        [IgnoreDataMember]
        private int id;
        [DataMember]
        private string vendorId;
        [DataMember]
        private string productId;
        [DataMember]
        private string serialId;
        [IgnoreDataMember]
        private DateTime? createdDate;
        [IgnoreDataMember]
        private DateTime? modifiedDate;


        public string SerialId
        {
            get
            {
                return serialId;
            }

            set
            {
                serialId = value;
            }
        }

        public override bool Equals(object obj)
        {
            var item = obj as PrinterDTO;

            if (item == null)
            {
                return false;
            }

            return this.VendorId.Equals(item.VendorId) && this.ProductId.Equals(item.ProductId) && this.SerialId.Equals(item.SerialId);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + VendorId.GetHashCode();
                hash = hash * 23 + ProductId.GetHashCode();
                hash = hash * 23 + SerialId.GetHashCode();
                return hash;
            }
        }

        [DataMember(Name = "created")]
        public string Created
        {
            get;
            set;
        }

        [DataMember(Name = "modified")]
        public string Modified
        {
            get;
            set;
        }

        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }

            set
            {
                createdDate = value;
            }
        }

        public DateTime? ModifiedDate
        {
            get
            {
                return modifiedDate;
            }

            set
            {
                modifiedDate = value;
            }
        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string VendorId
        {
            get
            {
                return vendorId;
            }

            set
            {
                vendorId = value;
            }
        }

        public string ProductId
        {
            get
            {
                return productId;
            }

            set
            {
                productId = value;
            }
        }

        [OnSerializing]
        void OnSerializing(StreamingContext context)
        {
            this.Created = DateTimeUtil.serializedDate(CreatedDate);
            this.Modified = DateTimeUtil.serializedDate(ModifiedDate);
        }

        [OnDeserialized]
        void OnDeserializing(StreamingContext context)
        {
            this.CreatedDate = DateTimeUtil.deserializeDate(Created);
            this.ModifiedDate = DateTimeUtil.deserializeDate(Modified);
        }
    }
}
