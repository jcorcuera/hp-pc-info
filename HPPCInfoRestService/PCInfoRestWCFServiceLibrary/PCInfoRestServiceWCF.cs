﻿using PCInfoRestServiceWCFLibrary.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace PCInfoRestServiceWCFLibrary
{
    public class PCInfoRestServiceWCF : IPCInfoRestService
    {

        public PCInfoDTO postData(PCInfoDTO pcInfoDTO)
        {
            SqlTransaction transaction = null;
            try
            {
                if (pcInfoDTO.HostName == "")
                {
                    throw new Exception("Invalid data input.");
                }
                string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection sqlConnection = new SqlConnection(connectionString))
                {
                    sqlConnection.Open();
                    transaction = sqlConnection.BeginTransaction("PCInfoTransaction");
                    PCInfoDTO currentPC = findPCInfo(pcInfoDTO.HostName, sqlConnection, transaction);
                    if (currentPC == null)
                    {
                        registerPCInfo(pcInfoDTO, sqlConnection, transaction);
                    }
                    else
                    {
                        updatePCInfo(currentPC, pcInfoDTO, sqlConnection, transaction);
                    }
                    transaction.Commit();
                }
                pcInfoDTO.OperationResult = true;
                return pcInfoDTO;
            }
            catch (Exception ex)
            {
                pcInfoDTO.OperationResult = false;
                pcInfoDTO.Message = FileLogger.GetExceptionDetails(ex);
                FileLogger.Log("Error while a request was processed.", ex);
                try
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }
                }
                catch (Exception ex2)
                {
                    // This catch block will handle any errors that may have occurred
                    // on the server that would cause the rollback to fail, such as
                    // a closed connection.
                    FileLogger.Log("Rollback Exception Message " + ex2.Message);
                }
            }
            return pcInfoDTO;
        }

        private PCInfoDTO findPCInfo(string pcName, SqlConnection sqlConnection, SqlTransaction transaction)
        {
            PCInfoDTO pcInfoDTO = null;
            using (SqlCommand sqlCommand = sqlConnection.CreateCommand())
            {
                sqlCommand.Transaction = transaction;
                sqlCommand.CommandType = CommandType.Text;
                sqlCommand.CommandText = "select id, name, created, modified from pc where name = @name";
                sqlCommand.Parameters.AddWithValue("@name", pcName);
                using (SqlDataReader reader = sqlCommand.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        pcInfoDTO = new PCInfoDTO();
                        pcInfoDTO.Id = reader.GetInt32(0);
                        pcInfoDTO.HostName = reader.GetString(1);
                        pcInfoDTO.CreatedDate = reader.GetDateTime(2);
                        if (!reader.IsDBNull(3))
                        {
                            pcInfoDTO.ModifiedDate = reader.GetDateTime(3);
                        }
                        
                    }
                }
            }
            if (pcInfoDTO != null)
            {
                pcInfoDTO.NetworkCards = listNICForPC(pcInfoDTO.Id, sqlConnection, transaction);
                pcInfoDTO.Printers = listPrinterForPC(pcInfoDTO.Id, sqlConnection, transaction);
            }
            return pcInfoDTO;
        }

        private List<NICDTO> listNICForPC(int pcId, SqlConnection sqlConnection, SqlTransaction transaction)
        {
            List<NICDTO> nicList = new List<NICDTO>();
            using (SqlCommand sqlCommand = sqlConnection.CreateCommand())
            {
                sqlCommand.Transaction = transaction;
                sqlCommand.CommandType = CommandType.Text;
                sqlCommand.CommandText = "select id, pc_id, description, mac_address, created, modified from nic where pc_id = @pc_id";
                sqlCommand.Parameters.AddWithValue("@pc_id", pcId);
                using (SqlDataReader reader = sqlCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        NICDTO nicDTO = new NICDTO();
                        nicDTO.Id = reader.GetInt32(0);
                        nicDTO.Description = reader.GetString(2);
                        nicDTO.MacAddress = reader.GetString(3);
                        nicDTO.CreatedDate = reader.GetDateTime(4);
                        if (!reader.IsDBNull(5))
                        {
                            nicDTO.ModifiedDate = reader.GetDateTime(5);
                        }
                        nicList.Add(nicDTO);
                    }
                }
            }
            return nicList;
        }

        private List<PrinterDTO> listPrinterForPC(int pcId, SqlConnection sqlConnection, SqlTransaction transaction)
        {
            List<PrinterDTO> printerList = new List<PrinterDTO>();
            using (SqlCommand sqlCommand = sqlConnection.CreateCommand())
            {
                sqlCommand.Transaction = transaction;
                sqlCommand.CommandType = CommandType.Text;
                sqlCommand.CommandText = "select id, pc_id, vendor_id, product_id, serial_id, created, modified from printer where pc_id = @pc_id";
                sqlCommand.Parameters.AddWithValue("@pc_id", pcId);
                using (SqlDataReader reader = sqlCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        PrinterDTO printerDTO = new PrinterDTO();
                        printerDTO.Id = reader.GetInt32(0);
                        printerDTO.VendorId = reader.GetString(2);
                        printerDTO.ProductId = reader.GetString(3);
                        printerDTO.SerialId = reader.GetString(4);
                        printerDTO.CreatedDate = reader.GetDateTime(5);
                        if (!reader.IsDBNull(6))
                        {
                            printerDTO.ModifiedDate = reader.GetDateTime(5);
                        }
                        printerList.Add(printerDTO);
                    }
                }
            }
            return printerList;
        }

        private PCInfoDTO updatePCInfo(PCInfoDTO currentPC, PCInfoDTO pcInfoDTO, SqlConnection sqlConnection, SqlTransaction transaction)
        {
            using (SqlCommand cmd = new SqlCommand("update pc set name = @name, modified = @modified where id = @id", sqlConnection))
            {
                cmd.Transaction = transaction;
                pcInfoDTO.ModifiedDate = DateTime.Now;
                cmd.Parameters.AddWithValue("@name", pcInfoDTO.HostName);
                cmd.Parameters.AddWithValue("@modified", pcInfoDTO.ModifiedDate);
                cmd.Parameters.AddWithValue("@id", currentPC.Id);
                cmd.ExecuteNonQuery();
            }
            updateNICList(currentPC, pcInfoDTO, sqlConnection, transaction);
            updatePrinterList(currentPC, pcInfoDTO, sqlConnection, transaction);
            return pcInfoDTO;
        }

        private void updateNICList(PCInfoDTO currentPC, PCInfoDTO pcInfoDTO, SqlConnection sqlConnection, SqlTransaction transaction)
        {
            List<NICDTO> currentNICList = currentPC.NetworkCards;
            List<NICDTO> newNICList = pcInfoDTO.NetworkCards;

            List<NICDTO> itemsToInsert = new List<NICDTO>();
            List<NICDTO> itemsToDelete = new List<NICDTO>();
            List<NICDTO> itemsToUpdate = new List<NICDTO>();
            if (newNICList == null || newNICList.Count == 0)
            {
                return;
            }
            if (currentNICList == null)
            {
                currentNICList = new List<NICDTO>();
            }
            foreach(NICDTO nic in currentNICList)
            {
                if (!newNICList.Contains(nic))
                {
                    itemsToDelete.Add(nic);
                }
            }
            foreach (NICDTO nic in newNICList)
            {
                if (!currentNICList.Contains(nic))
                {
                    itemsToInsert.Add(nic);
                }else
                {
                    NICDTO currentNIC = currentNICList[currentNICList.IndexOf(nic)];
                    nic.Id = currentNIC.Id;
                    itemsToUpdate.Add(nic);
                }
            }
            if (itemsToInsert.Count > 0)
            {
                foreach (NICDTO nic in itemsToInsert)
                {
                    registerNicInfo(currentPC.Id, nic, sqlConnection, transaction);
                }
            }
            /*if (itemsToDelete.Count > 0)
            {
                foreach (NICDTO nic in itemsToDelete)
                {
                    deleteNic(nic.Id, sqlConnection, transaction);
                }
            }*/
            if (itemsToUpdate.Count > 0)
            {
                foreach (NICDTO nic in itemsToUpdate)
                {
                    updateNicInfo(nic, sqlConnection, transaction);
                }
            }
        }

        private void updatePrinterList(PCInfoDTO currentPC, PCInfoDTO pcInfoDTO, SqlConnection sqlConnection, SqlTransaction transaction)
        {
            List<PrinterDTO> currentPrinterList = currentPC.Printers;
            List<PrinterDTO> newPrinterList = pcInfoDTO.Printers;

            List<PrinterDTO> itemsToInsert = new List<PrinterDTO>();
            List<PrinterDTO> itemsToUpdate = new List<PrinterDTO>();
            if (newPrinterList == null || newPrinterList.Count == 0)
            {
                return;
            }
            if (currentPrinterList == null)
            {
                currentPrinterList = new List<PrinterDTO>();
            }
            foreach (PrinterDTO printer in currentPrinterList)
            {
                if (newPrinterList.Contains(printer))
                {
                    itemsToUpdate.Add(printer);
                }
            }
            foreach (PrinterDTO printer in newPrinterList)
            {
                if (!currentPrinterList.Contains(printer))
                {
                    itemsToInsert.Add(printer);
                }
            }
            if (itemsToInsert.Count > 0)
            {
                List<VendorProductDTO> validVendorProducts = listValidVendorProducts(sqlConnection, transaction);
                foreach (PrinterDTO printer in itemsToInsert)
                {
                    foreach(VendorProductDTO item in validVendorProducts)
                    {
                        if (printer.VendorId == item.VendorId && printer.ProductId == item.ProductId)
                        {
                            registerPrinterInfo(currentPC.Id, printer, sqlConnection, transaction);
                        }
                    }
                }
            }
            if (itemsToUpdate.Count > 0)
            {
                foreach (PrinterDTO printer in itemsToUpdate)
                {
                    updatePrinterInfo(printer.Id, sqlConnection, transaction);
                }
            }
        }

        private PCInfoDTO registerPCInfo(PCInfoDTO pcInfoDTO, SqlConnection sqlConnection, SqlTransaction transaction)
        {
            using (SqlCommand cmd = new SqlCommand("insert into pc(name, created, modified) output INSERTED.ID VALUES(@name, @created, @created)", sqlConnection))
            {
                cmd.Transaction = transaction;
                pcInfoDTO.CreatedDate = DateTime.Now;
                cmd.Parameters.AddWithValue("@name", pcInfoDTO.HostName);
                cmd.Parameters.AddWithValue("@created", pcInfoDTO.CreatedDate);
                int newId = (int)cmd.ExecuteScalar();
                pcInfoDTO.Id = newId;
                if (pcInfoDTO.NetworkCards.Count > 0)
                {
                    foreach(NICDTO nic in pcInfoDTO.NetworkCards)
                    {
                        registerNicInfo(pcInfoDTO.Id, nic, sqlConnection, transaction);
                    }
                }
                if (pcInfoDTO.Printers.Count > 0)
                {
                    List<VendorProductDTO> validVendorProducts = listValidVendorProducts(sqlConnection, transaction);
                    foreach (PrinterDTO printer in pcInfoDTO.Printers)
                    {
                        foreach (VendorProductDTO item in validVendorProducts)
                        {
                            if (printer.VendorId == item.VendorId && printer.ProductId == item.ProductId)
                            {
                                registerPrinterInfo(pcInfoDTO.Id, printer, sqlConnection, transaction);
                            }
                        }
                    }
                }
            }
            return pcInfoDTO;
        }

        private PrinterDTO registerPrinterInfo(int pcId, PrinterDTO printerDTO, SqlConnection sqlConnection, SqlTransaction transaction)
        {
            using (SqlCommand cmd = new SqlCommand("insert into printer(pc_id, vendor_id, product_id, serial_id, created, modified) output INSERTED.ID VALUES(@pc_id, @vendor_id, @product_id, @serial_id, @created, @created)", sqlConnection))
            {
                cmd.Transaction = transaction;
                printerDTO.CreatedDate = DateTime.Now;
                cmd.Parameters.AddWithValue("@pc_id", pcId);
                cmd.Parameters.AddWithValue("@vendor_id", printerDTO.VendorId);
                cmd.Parameters.AddWithValue("@product_id", printerDTO.ProductId);
                cmd.Parameters.AddWithValue("@serial_id", printerDTO.SerialId);
                cmd.Parameters.AddWithValue("@created", printerDTO.CreatedDate);
                int newId = (int)cmd.ExecuteScalar();
                printerDTO.Id = newId;
            }
            return printerDTO;
        }

        private NICDTO registerNicInfo(int pcId, NICDTO nicDTO, SqlConnection sqlConnection, SqlTransaction transaction)
        {
            using (SqlCommand cmd = new SqlCommand("insert into  nic(pc_id, description, mac_address, created, modified) output INSERTED.ID VALUES(@pc_id, @description, @mac_address, @created, @created)", sqlConnection))
            {
                cmd.Transaction = transaction;
                nicDTO.CreatedDate = DateTime.Now;
                cmd.Parameters.AddWithValue("@pc_id", pcId);
                cmd.Parameters.AddWithValue("@description", nicDTO.Description);
                cmd.Parameters.AddWithValue("@mac_address", nicDTO.MacAddress);
                cmd.Parameters.AddWithValue("@created", nicDTO.CreatedDate);
                int newId = (int)cmd.ExecuteScalar();
                nicDTO.Id = newId;
            }
            return nicDTO;
        }

        private NICDTO updateNicInfo(NICDTO nicDTO, SqlConnection sqlConnection, SqlTransaction transaction)
        {
            using (SqlCommand cmd = new SqlCommand("update nic set description = @description, modified = @modified where id = @id", sqlConnection))
            {
                cmd.Transaction = transaction;
                nicDTO.ModifiedDate = DateTime.Now;
                cmd.Parameters.AddWithValue("@id", nicDTO.Id);
                cmd.Parameters.AddWithValue("@description", nicDTO.Description);
                cmd.Parameters.AddWithValue("@modified", nicDTO.ModifiedDate);
                cmd.ExecuteNonQuery();
            }
            return nicDTO;
        }

        private void deleteNic(int nicId, SqlConnection sqlConnection, SqlTransaction transaction)
        {
            using (SqlCommand sqlCommand = sqlConnection.CreateCommand())
            {
                sqlCommand.Transaction = transaction;
                sqlCommand.CommandType = CommandType.Text;
                sqlCommand.CommandText = "delete from nic where id = @id";
                sqlCommand.Parameters.AddWithValue("@id", nicId);
                sqlCommand.ExecuteNonQuery();
            }
        }

        private void updatePrinterInfo(int printerId, SqlConnection sqlConnection, SqlTransaction transaction)
        {
            using (SqlCommand sqlCommand = sqlConnection.CreateCommand())
            {
                sqlCommand.Transaction = transaction;
                sqlCommand.CommandType = CommandType.Text;
                sqlCommand.CommandText = "update printer set modified = @modifiedDate where id = @id";
                sqlCommand.Parameters.AddWithValue("@id", printerId);
                sqlCommand.Parameters.AddWithValue("@modifiedDate", DateTime.Now);
                sqlCommand.ExecuteNonQuery();
            }
        }

        private void deletePrinter(int printerId, SqlConnection sqlConnection, SqlTransaction transaction)
        {
            using (SqlCommand sqlCommand = sqlConnection.CreateCommand())
            {
                sqlCommand.Transaction = transaction;
                sqlCommand.CommandType = CommandType.Text;
                sqlCommand.CommandText = "delete from printer where id = @id";
                sqlCommand.Parameters.AddWithValue("@id", printerId);
                sqlCommand.ExecuteNonQuery();
            }
        }

        private List<VendorProductDTO> listValidVendorProducts(SqlConnection sqlConnection, SqlTransaction transaction)
        {
            List<VendorProductDTO> results = new List<VendorProductDTO>();
            using (SqlCommand sqlCommand = sqlConnection.CreateCommand())
            {
                sqlCommand.Transaction = transaction;
                sqlCommand.CommandType = CommandType.Text;
                sqlCommand.CommandText = "select v.vendor_id, p.product_id from product p inner join vendor v on v.id = p.vendor_id";
                using (SqlDataReader reader = sqlCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        VendorProductDTO result = new VendorProductDTO();
                        result.VendorId = reader.GetString(0);
                        result.ProductId = reader.GetString(1);
                        results.Add(result);
                    }
                }
            }
            return results;
        }
    }
}